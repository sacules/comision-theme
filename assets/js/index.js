function showmenu(x) {
  var nav = document.getElementById("topNav");
  if (nav.className == "") {
    nav.className = "responsive";
  } else {
    nav.className = "";
  }

  var links = document.getElementById("navLinks");
  if (links.style.display == "block") {
    links.style.display = "none";
  } else {
    links.style.display = "block";
  }

  x.classList.toggle("change");
}
